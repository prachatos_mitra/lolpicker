function resetCurrent() {
	$("#champ").find("input").val("");
	var defImg;
	for (var i = 1; i <=5; ++i) {
		if (i == 1) {
			defImg = "./images/top.png";
		} else if (i == 2) {
			defImg = "./images/jg.png";
		} else if (i == 3) {
			defImg = "./images/mid.png";
		} else if (i == 4) {
			defImg = "./images/bot.png";
		} else if (i == 5) {
			defImg = "./images/sup.png";
		}		
		$("#champ" + i).find("img").attr("src", defImg);
		$("#champ" + i).find("img").attr("class", "champ");
		$(".team-name").val("My Team 1");
		$(".mages").find("img").css("filter", "");
		$(".adcs").find("img").css("filter", "");
		$(".fighters").find("img").css("filter", "");
		$(".tanks").find("img").css("filter", "");
		$(".supports").find("img").css("filter", "");
		$(".assassins").find("img").css("filter", "");
		$("#analysis").css("visibility", "hidden");
	}
}

function generateXML(champID,  champName, champRole) {
	var xmldoc = document.implementation.createDocument(null, "team");
	
	var topE = document.createElementNS(null, 'top');
	topE.setAttribute('role', champRole[1]);
	var topId =  document.createElementNS(null, 'top_champion_id');
	var topIdText = document.createTextNode(champID[1]);
    topId.appendChild(topIdText);
	var topName =  document.createElementNS(null, 'top_champion_name');
	var topNameText = document.createTextNode(champName[1]);
    topName.appendChild(topNameText);
	topE.appendChild(topId);
	topE.appendChild(topName);
	xmldoc.documentElement.appendChild(topE);
	
	var jungleE = document.createElementNS(null, 'jungle');
	jungleE.setAttribute('role', champRole[2]);
	var jungleId =  document.createElementNS(null, 'jungle_champion_id');
	var jungleIdText = document.createTextNode(champID[2]);
    jungleId.appendChild(jungleIdText);
	var jungleName =  document.createElementNS(null, 'jungle_champion_name');
	var jungleNameText = document.createTextNode(champName[2]);
    jungleName.appendChild(jungleNameText);
	jungleE.appendChild(jungleId);
	jungleE.appendChild(jungleName);
	xmldoc.documentElement.appendChild(jungleE);
	
	var midE = document.createElementNS(null, 'mid');
	midE.setAttribute('role', champRole[3]);
	var midId =  document.createElementNS(null, 'mid_champion_id');
	var midIdText = document.createTextNode(champID[3]);
    midId.appendChild(midIdText);
	var midName =  document.createElementNS(null, 'mid_champion_name');
	var midNameText = document.createTextNode(champName[3]);
    midName.appendChild(midNameText);
	midE.appendChild(midId);
	midE.appendChild(midName);
	xmldoc.documentElement.appendChild(midE);
	
	var botE = document.createElementNS(null, 'bot');
	botE.setAttribute('role', champRole[4]);
	var botId =  document.createElementNS(null, 'bot_champion_id');
	var botIdText = document.createTextNode(champID[4]);
    botId.appendChild(botIdText);
	var botName =  document.createElementNS(null, 'bot_champion_name');
	var botNameText = document.createTextNode(champName[4]);
    botName.appendChild(botNameText);
	botE.appendChild(botId);
	botE.appendChild(botName);
	xmldoc.documentElement.appendChild(botE);
	
	var supportE = document.createElementNS(null, 'support');
	supportE.setAttribute('role', champRole[5]);
	var supportId =  document.createElementNS(null, 'support_champion_id');
	var supportIdText = document.createTextNode(champID[5]);
    supportId.appendChild(supportIdText);
	var supportName =  document.createElementNS(null, 'support_champion_name');
	var supportNameText = document.createTextNode(champName[5]);
    supportName.appendChild(supportNameText);
	supportE.appendChild(supportId);
	supportE.appendChild(supportName);
	xmldoc.documentElement.appendChild(supportE);
}

$(document).ready(function() {

	$(".draggable").draggable({
		containment: "window",
		revert: true,
		zIndex: 100
	});
	$('.dropdown-submenu a.load-saved').on("mouseover", function(e){
		$.ajax({
            type: "POST",
			url: "http://localhost/lolpicker/api/list_teams.cgi",
			dataType: "json",
			success: function(sr) {
				var count = parseInt(sr["count"]);
				if (!count || count <= 0) {
					alert("No saved teams!");
					return;
				}
				var i;
				$("#loadsave").empty();
				for (entry in sr) {
					if (parseInt(entry)) {
						$("#loadsave").prepend("<li><a tabindex='-1' id='" + parseInt(entry) + "'class='team-list'>" + sr[entry] + "</li>");
					}
				}
				
			}
		});
		$(this).next('ul').toggle();
		e.stopPropagation();
		e.preventDefault();
	});
	
	$(".analyze").click(function() {
		var champID = [];
		var champRole = [];
		var champName = [];
		for (i = 1; i <= 5; ++i) {
			champID[i] = parseInt($("#champ" + i).find("input").val());
			if (!champID[i]) champID[i] = 0;
			var classList = $("#champ" + i).find("img").attr("class");
			if (classList.toLowerCase().includes("assassin")) {
				champRole[i] = "assassin"
			} else if (classList.toLowerCase().includes("adc")) {
				champRole[i] = "adc"
			} else if (classList.toLowerCase().includes("fighter")) {
				champRole[i] = "fighter"
			} else if (classList.toLowerCase().includes("tank")) {
				champRole[i] = "tank"
			} else if (classList.toLowerCase().includes("support")) {
				champRole[i] = "support"
			} else if (classList.toLowerCase().includes("mage")) {
				champRole[i] = "mage"
			} else {
				alert("Complete your team before analysis!");
				return;
			}
			champName[i] = $("." + champRole[i] + "s").find("input[value=" + champID[i] + "]").parent().attr("data-original-title");
		}
		var problems = "";
		//Try Champion.gg
		var key = "ab1edf6d149914a60f4a4e4b925302f9"; //sorry not sorry
		var champGGScore = 0;
		var champGGCount = 0;
		for (var i = 1; i <= 5; ++i) {
			$.ajax({
				url: "http://api.champion.gg/v2/champions/" + champID[i] + "?champData=winRate&api_key=" + key,
				async: false,
				success: function (data){
						var curWR = 0;
						var len = data.length;
						for (var i = 0; i < len; ++i) {
							curWR += data[i]['winRate'];
						}
						curWR = curWR / len * 100;
						if (curWR) {
							champGGCount = champGGCount + 1;
							champGGScore = champGGScore + curWR;
						}
					}
			});
		}
		if (champGGCount) champGGScore = champGGScore / champGGCount;
		if (champGGScore) problems = "Champion.GG API claims average winrate is " + champGGScore.toFixed(2) + "%.<br/>";
		if (champRole[4] != "adc" && champRole[3] != "adc") {
			problems = problems + " No ranged AD threats in team.";
		}
		if (champRole[1] != "mage" && champRole[2] != "mage" && champRole[3] != "mage" && champRole[4] != "mage" 
					&& champID[1] != 27 && champID[2] != 27 && champID[3] != 27 && champID[4] != 27 && champID[5] != 27 ) {
			problems = problems + " No ranged AP threats in team.";
		}
		var tank = false;
		if (champRole[1] != "tank" && champRole[2] != "tank" && champRole[3] != "tank" && champRole[4] != "tank" && champRole[5] != "tank") {
			for (var i = 1; i <= 5; i++) {
				if (champID[i] == 16 || champID[i] == 19 || champID[16] == 15) {
					tank = true;
					break;
				}
			}
			if (!tank) problems = problems + " No frontline in team.";
		}
		if (champRole[5] != "support" && champRole[5] != "tank") {
			problems = problems + " No supportive champions.";
		}
		var assassin = true;
		for (var i = 1; i <= 5; i++) {
			if (champRole[i] != "assassin") {
				assassin = false;
				break;
			}
		}
		if (assassin) problems = problems + " Bronze 5 team composition.";
		if (!problems) problems = "All in order!";
		$("#analysis").html(problems);
		$("#analysis").css("visibility", "visible");
	});
	
	$(".droppable").droppable({
		accept: ".draggable",
		hoverClass: "drop-hover",
		drop: function( event, ui ) {
			var droppedAt = $(this).attr('id').replace("champ","");
			if ($(ui.draggable).is('img')) {
				//replacement case;
				var curImg = $(this).find('img').attr('src');				
				var image = $(ui.draggable).attr('src');
				var curId = $(this).find(".champ-id").val();
				var droppedType = $(ui.draggable).attr('class').replace('champ ','');	
				if (parseInt(curId) >= 1) {
					//swap
					$(ui.draggable).attr('src', curImg);
					$(this).find("img").attr('src', image);
					$(this).find(".champ-id").val($(ui.draggable).parent().find(".champ-id").val());
					$(this).find("img").attr('class','champ ' + droppedType);
					$(ui.draggable).parent().find(".champ-id").val(curId);
				} else {
					//one empty, one filled
					
					var otherID = $(ui.draggable).parent().attr('id');
					otherID = parseInt(otherID.replace('champ', ''));
					var defImg;
					if (otherID == 1) {
						defImg = "./images/top.png";
					} else if (otherID == 2) {
						defImg = "./images/jg.png";
					} else if (otherID == 3) {
						defImg = "./images/mid.png";
					} else if (otherID == 4) {
						defImg = "./images/bot.png";
					} else if (otherID == 5) {
						defImg = "./images/sup.png";
					} else {
						return;
					}
					
					$(this).find("img").attr('src', image);
					$(ui.draggable).attr('src', defImg);
					
					$(this).find(".champ-id").val($(ui.draggable).parent().find(".champ-id").val());
					$(ui.draggable).parent().find(".champ-id").val(curId);	
					$(this).find("img").attr('class','champ ' + droppedType);
					$(ui.draggable).attr('class', 'champ');
					$( ui.draggable ).draggable({
					  cancel: ".title"
					});
					$(ui.draggable).draggable('option', 'cancel', '.title');
					var classesCur = $(this).find("img").attr('class');
					if (classesCur.indexOf("draggable") == -1) {
						classesCur = classesCur + " draggable ui-draggable ui-draggable-handle";
					}
					
					$(this).find("img").draggable({
						containment: "window",
						revert: true,
						zIndex: 100
					});
					
					$(this).find("img").attr('class', classesCur);
				}
				return;
			}
				
			var droppedType = $(ui.draggable).find("img").attr('class').replace('champ ','');	
			var image = $(ui.draggable).find("img").attr('src');
			var i, exists = false;
			for(i = 1; i<=5;i++) {
				if(i != droppedAt && $("#champ"+i).find("img").attr('src') == image) {
					return;
				}
			}
			if(!exists) {
				var curImg = $(this).find('img').attr('src');
				if (curImg == image) return;
				$("img[src='" + image + "']").css("filter", "grayscale(100%)");
				$("img[src='" + curImg + "']").css("filter", "");
				$(this).find("img").attr('src',image);
				
				$(this).find(".champ-id").val($(ui.draggable).find("input").val());
				$(this).find("img").attr('class','champ ' + droppedType);
				var classesCur = $(this).find("img").attr('class');
				if (classesCur.indexOf("draggable") == -1) {
					classesCur = classesCur + " draggable ui-draggable ui-draggable-handle";
				}
				$(this).find("img").draggable({
					containment: "window",
					revert: true,
					zIndex: 100
				});
				$(this).find("img").attr('class', classesCur);
			}
			
		}
	});
	
	function replaceActive(prevEl, curEl) {
		//get previous active
		var prevClass = prevEl.html().toLowerCase();
		var newClass = curEl.html().toLowerCase();
		var prevClassSing = prevClass.substring(0, prevClass.length - 1);
		var newClassSing = newClass.substring(0, newClass.length - 1);
		if (prevClass == "marksmen") {
			prevClass = "adcs";
			prevClassSing = "adc";
		}
		if (newClass == "marksmen") {
			newClass = "adcs";
			newClassSing = "adc";
		}
		$("." + prevClass).attr('class', 'row ' + prevClass + ' collapse');
		$(".active").attr('class', 'col-md-2 type ' + prevClassSing + ' ' + prevClassSing + '-head');
		$("." + newClass).attr('class', 'row ' + newClass);
		curEl.attr('class', 'col-md-2 type ' + newClassSing + ' ' + newClassSing + '-head active');
	}
	
	//enables tooltips
	$('[data-toggle="tooltip"]').tooltip();

	$(".type").click(function() {
		var prev = $(".active");
		replaceActive(prev, $(this));
	});
	
	$(".load").click(function() {
		$.ajax({
            type: "POST",
			url: "http://localhost/lolpicker/api/list_teams.cgi",
			dataType: "json",
			success: function(sr) {
				var count = parseInt(sr["count"]);
				if (!count || count <= 0) {
					alert("No saved teams!");
					return;
				}
				var i;
				$("#load-panel").css("display", "block");
				$("#load-panel").empty();
				for (entry in sr) {
					if (parseInt(entry)) {
						$("#load-panel").prepend("<p style='color: #9e7b37; font-size: 20px; font-weight: bold; cursor: pointer;' id='" + parseInt(entry) + "'class='team-list'>" + sr[entry] + "</li>");
					}
				}
				$("#load-panel").off("resize");
				
			}
		});
	});
	$(document).on("click", ".team-list", function() {
		$.ajax({
			type: "GET",
			url: "http://localhost/lolpicker/api/load_team.cgi",
			data: {	id: $(this).attr("id") },
			dataType: "json",
			success: function(sr) {
					if (sr['success'] == 1) {
						var XML = sr['team_content'];
						if (!XML) {
							alert("Invalid load file!");
							return;
						}
						
						//Parse received data
						var parsedXML = $.parseXML(XML),
						$xml = $(parsedXML);
						
						//Parse stored champion data
						var xmlc;
						$.ajax({
							url: "./res/champions.xml",
							async: false,
							success: function (data){
								xmlc = new XMLSerializer().serializeToString(data.documentElement);
							}
						});
						var champXML = $.parseXML(xmlc),
						$srcXML = $(champXML);
						if (!$srcXML || !$xml) return;
						
						//ok, good to go
						//reset the current stuff
						resetCurrent();
						
						//load top
						var $top = $xml.find("top");
						var top_id = parseInt($top.find("top_champion_id").text());
						var top_role = $top.attr("role");
						if (!top_id || !top_role) {
							alert("Invalid load file!");
							return;
						}
						var topImg = './images/' +  $srcXML.find(top_role + '[id=' + top_id + ']').attr("img");
						$("img[src='" + topImg + "']").css("filter", "grayscale(100%)");
						$("#champ1").find("img").attr("src", topImg);
						$("#champ1").find(".champ-id").val(top_id);
						$("#champ1").find("img").attr('class','champ ' + top_role);
						var classesCur = $("#champ1").find("img").attr('class');
						if (classesCur.indexOf("draggable") == -1) {
							classesCur = classesCur + " draggable ui-draggable ui-draggable-handle";
						}
						$("#champ1").find("img").draggable({
							containment: "window",
							revert: true,
							zIndex: 100
						});
						$("#champ1").find("img").attr('class', classesCur);

						//Jg
						var $jg = $xml.find("jungle");
						var jg_id = parseInt($jg.find("jungle_champion_id").text());
						var jg_role = $jg.attr("role");
						if (!jg_id || !jg_role) {
							resetCurrent();
							alert("Invalid load file!");
							return;
						}
						var jgImg = './images/' +  $srcXML.find(jg_role + '[id=' + jg_id + ']').attr("img");
						$("img[src='" + jgImg + "']").css("filter", "grayscale(100%)");
						$("#champ2").find("img").attr("src", jgImg);
						$("#champ2").find(".champ-id").val(jg_id);
						$("#champ2").find("img").attr('class','champ ' + jg_role);
						var classesCur = $("#champ2").find("img").attr('class');
						if (classesCur.indexOf("draggable") == -1) {
							classesCur = classesCur + " draggable ui-draggable ui-draggable-handle";
						}
						$("#champ2").find("img").draggable({
							containment: "window",
							revert: true,
							zIndex: 100
						});
						$("#champ2").find("img").attr('class', classesCur);
						
						//Mid
						var $mid = $xml.find("mid");
						var mid_id = parseInt($mid.find("mid_champion_id").text());
						var mid_role = $mid.attr("role");
						if (!mid_id || !mid_role) {
							resetCurrent();
							alert("Invalid load file!");
							return;
						}
						var midImg = './images/' +  $srcXML.find(mid_role + '[id=' + mid_id + ']').attr("img");
						$("img[src='" + midImg + "']").css("filter", "grayscale(100%)");
						$("#champ3").find("img").attr("src", midImg);
						$("#champ3").find(".champ-id").val(mid_id);
						$("#champ3").find("img").attr('class','champ ' + mid_role);
						var classesCur = $("#champ3").find("img").attr('class');
						if (classesCur.indexOf("draggable") == -1) {
							classesCur = classesCur + " draggable ui-draggable ui-draggable-handle";
						}
						$("#champ3").find("img").draggable({
							containment: "window",
							revert: true,
							zIndex: 100
						});
						$("#champ3").find("img").attr('class', classesCur);
						
						//Bot
						var $bot = $xml.find("bot");
						var bot_id = parseInt($bot.find("bot_champion_id").text());
						var bot_role = $bot.attr("role");
						if (!bot_id || !bot_role) {
							resetCurrent();
							alert("Invalid load file!");
							return;
						}
						var botImg = './images/' +  $srcXML.find(bot_role + '[id=' + bot_id + ']').attr("img");
						$("img[src='" + botImg + "']").css("filter", "grayscale(100%)");
						$("#champ4").find("img").attr("src", botImg);
						$("#champ4").find(".champ-id").val(bot_id);
						$("#champ4").find("img").attr('class','champ ' + bot_role);
						var classesCur = $("#champ4").find("img").attr('class');
						if (classesCur.indexOf("draggable") == -1) {
							classesCur = classesCur + " draggable ui-draggable ui-draggable-handle";
						}
						$("#champ4").find("img").draggable({
							containment: "window",
							revert: true,
							zIndex: 100
						});
						$("#champ4").find("img").attr('class', classesCur);
						
						//Supp
						var $sup = $xml.find("support");
						var sup_id = parseInt($sup.find("support_champion_id").text());
						var sup_role = $sup.attr("role");
						if (!sup_id || !sup_role) {
							resetCurrent();
							alert("Invalid load file!");
							return;
						}
						var supImg = './images/' +  $srcXML.find(sup_role + '[id=' + sup_id + ']').attr("img");
						$("img[src='" + supImg + "']").css("filter", "grayscale(100%)");
						$("#champ5").find("img").attr("src", supImg);
						$("#champ5").find(".champ-id").val(sup_id);
						$("#champ5").find("img").attr('class','champ ' + sup_role);
						var classesCur = $("#champ5").find("img").attr('class');
						if (classesCur.indexOf("draggable") == -1) {
							classesCur = classesCur + " draggable ui-draggable ui-draggable-handle";
						}
						$("#champ5").find("img").draggable({
							containment: "window",
							revert: true,
							zIndex: 100
						});
						$("#champ5").find("img").attr('class', classesCur);
						 $(".team-name").html(sr['team_name']);
					}
				}
		});
	});
	
	$(".save").click(function(){
		var i;
		var saveName = $(".team-name").val();
		if (saveName.length <= 0) {
			alert("Please give a name to your team!");
			return;
		}
		var champID = [];
		var champRole = [];
		var champName = [];
		for (i = 1; i <= 5; ++i) {
			champID[i] = parseInt($("#champ" + i).find("input").val());
			if (!champID[i]) champID[i] = 0;
			var classList = $("#champ" + i).find("img").attr("class");
			if (classList.toLowerCase().includes("assassin")) {
				champRole[i] = "assassin"
			} else if (classList.toLowerCase().includes("adc")) {
				champRole[i] = "adc"
			} else if (classList.toLowerCase().includes("fighter")) {
				champRole[i] = "fighter"
			} else if (classList.toLowerCase().includes("tank")) {
				champRole[i] = "tank"
			} else if (classList.toLowerCase().includes("support")) {
				champRole[i] = "support"
			} else if (classList.toLowerCase().includes("mage")) {
				champRole[i] = "mage"
			} else {
				alert("Complete your team before saving!");
				return;
			}
			champName[i] = $("." + champRole[i] + "s").find("input[value=" + champID[i] + "]").parent().attr("data-original-title");
		}
		var data = [];
		for (i = 1; i <= 5; ++i) {
			data[i - 1] = { id: champID[i], role: champRole[i], name: champName[i] };
		}
		$.ajax({
            type: "POST",
			url: "http://localhost/lolpicker/api/save_team.cgi",
			data: {
				team_name: saveName,
				team_content_0: JSON.stringify({ id: champID[1], role: champRole[1], name: champName[1] }),
				team_content_1: JSON.stringify({ id: champID[2], role: champRole[2], name: champName[2] }),
				team_content_2: JSON.stringify({ id: champID[3], role: champRole[3], name: champName[3] }),
				team_content_3: JSON.stringify({ id: champID[4], role: champRole[4], name: champName[4] }),
				team_content_4: JSON.stringify({ id: champID[5], role: champRole[5], name: champName[5] }),			
			},
			dataType: "json",
			success: function(sr) {
				if (sr.success == 1) {
					alert("Saved successfully!");
				} else {
					alert("Could not save to database. Please check errors and try again");
				}
			}
		});
			
	});
	
	$(".reset").click(function() {
		resetCurrent();
	});
});