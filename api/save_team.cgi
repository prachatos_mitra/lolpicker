#!D:\xampp\perl\bin\perl.exe
# The above line is perl execution path in xampp
# The below line tells the browser, that this script will send html content.
# If you miss this line then it will show "malformed header from script" error.
use DBI;

print "Content-type:application/json\r\n\r\n";
local ($buffer, @pairs, $pair, $name, $value, %FORM);
# Read in text
$ENV{'REQUEST_METHOD'} =~ tr/a-z/A-Z/;
if ($ENV{'REQUEST_METHOD'} eq "POST") {
   read(STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
}else {
   $buffer = $ENV{'QUERY_STRING'};
}
# Split information into name/value pairs
@pairs = split(/&/, $buffer);
foreach $pair (@pairs) {
   ($name, $value) = split(/=/, $pair);
   $value =~ tr/+/ /;
   $value =~ s/%(..)/pack("C", hex($1))/eg;
   $FORM{$name} = $value;
}
$team_name = $FORM{team_name};
$team_top  = $FORM{team_content_0};
$team_jg  = $FORM{team_content_1};
$team_mid = $FORM{team_content_2};
$team_bot  = $FORM{team_content_3};
$team_sup  = $FORM{team_content_4};
use JSON;
my $top_id = @{decode_json($team_top)}{'id'};
my $jg_id = @{decode_json($team_jg)}{'id'};
my $mid_id = @{decode_json($team_mid)}{'id'};
my $bot_id = @{decode_json($team_bot)}{'id'};
my $sup_id = @{decode_json($team_sup)}{'id'};

my $top_name = @{decode_json($team_top)}{'name'};
my $jg_name = @{decode_json($team_jg)}{'name'};
my $mid_name = @{decode_json($team_mid)}{'name'};
my $bot_name = @{decode_json($team_bot)}{'name'};
my $sup_name = @{decode_json($team_sup)}{'name'};

my $top_role = @{decode_json($team_top)}{'role'};
my $jg_role = @{decode_json($team_jg)}{'role'};
my $mid_role = @{decode_json($team_mid)}{'role'};
my $bot_role = @{decode_json($team_bot)}{'role'};
my $sup_role = @{decode_json($team_sup)}{'role'};

my $driver = "mysql"; 
my $database = "teamdb";
my $dsn = "DBI:$driver:database=$database";
my $userid = "root";
my $password = "password";
use XML::LibXML;
my $doc = XML::LibXML::Document->new('1.0', 'utf-8');
my $root = $doc->createElement("team");

$root->setAttribute('name'=> $team_name);

#Top
my $el = $doc->createElement("top");
$el->setAttribute('role' => $top_role);
$tag = $doc->createElement("top_champion_id");
$tag->appendTextNode($top_id);
$el->appendChild($tag);
$tag = $doc->createElement("top_champion_name");
$tag->appendTextNode($top_name);
$el->appendChild($tag);
$root->appendChild($el);

#Jg
my $el = $doc->createElement("jungle");
$el->setAttribute('role' => $jg_role);
$tag = $doc->createElement("jungle_champion_id");
$tag->appendTextNode($jg_id);
$el->appendChild($tag);
$tag = $doc->createElement("jungle_champion_name");
$tag->appendTextNode($jg_name);
$el->appendChild($tag);
$root->appendChild($el);

#Mid
my $el = $doc->createElement("mid");
$el->setAttribute('role' => $mid_role);
$tag = $doc->createElement("mid_champion_id");
$tag->appendTextNode($mid_id);
$el->appendChild($tag);
$tag = $doc->createElement("mid_champion_name");
$tag->appendTextNode($mid_name);
$el->appendChild($tag);
$root->appendChild($el);

#Bot
my $el = $doc->createElement("bot");
$el->setAttribute('role' => $bot_role);
$tag = $doc->createElement("bot_champion_id");
$tag->appendTextNode($bot_id);
$el->appendChild($tag);
$tag = $doc->createElement("bot_champion_name");
$tag->appendTextNode($bot_name);
$el->appendChild($tag);
$root->appendChild($el);

#Sup
my $el = $doc->createElement("support");
$el->setAttribute('role' => $sup_role);
$tag = $doc->createElement("support_champion_id");
$tag->appendTextNode($sup_id);
$el->appendChild($tag);
$tag = $doc->createElement("support_champion_name");
$tag->appendTextNode($sup_name);
$el->appendChild($tag);
$root->appendChild($el);
$doc->setDocumentElement($root);
my $team_content = $doc->toString();
$team_content =~ s/\\//g;
my %json = (
	success => 1,
);
my $dbh = DBI->connect($dsn, $userid, $password ) or $json{'success'} = 0;
my $sth = $dbh->prepare("INSERT INTO `team_data`(`team_name`, `team_content`)
                         values
                      (?, ?)");
$sth->execute($team_name, $team_content) or $json{'success'} = 0;
$sth->finish();
print encode_json \%json;

1;