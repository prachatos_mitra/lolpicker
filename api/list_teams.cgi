#!D:\xampp\perl\bin\perl.exe
# The above line is perl execution path in xampp
# The below line tells the browser, that this script will send html content.
# If you miss this line then it will show "malformed header from script" error.
use DBI;

use JSON;
print "Content-type:application/json\r\n\r\n";
my $driver = "mysql"; 
my $database = "teamdb";
my $dsn = "DBI:$driver:database=$database";
my $userid = "root";
my $password = "password";

my $done = 1;
my %json = (
	count => 0,
);
my $dbh = DBI->connect($dsn, $userid, $password ) or $json{'count'} = -1;
my $sth = $dbh->prepare("SELECT `team_id`, `team_name` FROM `team_data` ORDER BY `team_timestamp` ASC");
$sth->execute() or $json{'count'} = -1;
my $rows = $sth->rows;
my $i = 0;
$json{'count'} = $rows;
while (my @row = $sth->fetchrow_array()) {
	my $team_id = $row[0];
	my $team_name = $row[1];
	$json{$team_id} = $team_name;
	$i = $i + 1;
}
$sth->finish();
print encode_json \%json;
1;