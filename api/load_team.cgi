#!D:\xampp\perl\bin\perl.exe
# The above line is perl execution path in xampp
# The below line tells the browser, that this script will send html content.
# If you miss this line then it will show "malformed header from script" error.
use DBI;

print "Content-type:application/json\r\n\r\n";
local ($buffer, @pairs, $pair, $name, $value, %FORM);
# Read in text
$ENV{'REQUEST_METHOD'} =~ tr/a-z/A-Z/;
if ($ENV{'REQUEST_METHOD'} eq "GET") {
   $buffer = $ENV{'QUERY_STRING'};
}
# Split information into name/value pairs
@pairs = split(/&/, $buffer);
foreach $pair (@pairs) {
   ($name, $value) = split(/=/, $pair);
   $value =~ tr/+/ /;
   $value =~ s/%(..)/pack("C", hex($1))/eg;
   $FORM{$name} = $value;
}
$team_id = $FORM{id};

my $driver = "mysql"; 
my $database = "teamdb";
my $dsn = "DBI:$driver:database=$database";
my $userid = "root";
my $password = "password";

my %json = (
	success => 1,
	team_name => 0,
	team_content => 0,
);
my $dbh = DBI->connect($dsn, $userid, $password ) or $json{'success'} = 0;
my $sth = $dbh->prepare("SELECT `team_name`, `team_content` FROM `team_data` WHERE `team_id` = ?");
$sth->execute($team_id) or $json{'success'} = 0;
my $rows = $sth->rows;
while (my @row = $sth->fetchrow_array()) {
	my $team_name = $row[0];
	my $team_content = $row[1];
	$json{'team_name'} = $team_name;
	$json{'team_content'} = $team_content;
}
if ($rows == 0) {
	$json{'success'} = 0;
}
$sth->finish();
use JSON;
print encode_json \%json;
1;